# Kiwi Flight Search

Flights search web app using kiwi.com APIs

### Live demo
[tomas223.gitlab.io/kiwi-flight-search/](https://tomas223.gitlab.io/kiwi-flight-search/)

### Prerequisites
node.js

### Install & Start

```
git clone https://gitlab.com/tomas223/kiwi-flight-search.git
cd kiwi-flight-search
npm i
npm start
```

Go to:
[localhost:3000](http://localhost:3000/)