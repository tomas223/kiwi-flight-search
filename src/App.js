import React, {Component} from 'react';
import {Container} from 'reactstrap';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-select/dist/react-select.css';
import './scss/App.scss';
import SearchPage from './components/SearchPage';

class App extends Component {

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <Container>
                        <h1>Kiwi Flight Search</h1>
                    </Container>
                </header>
                <main>
                    <SearchPage />
                </main>
            </div>
        );
    }
}

export default App;
