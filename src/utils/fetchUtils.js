const BASE_URL = "https://api.skypicker.com/";
const DATE_FORMAT = "DD/MM/YYYY";

export async function getRequest(url) {
    console.log("Requesting: ", url);
    return await (await (fetch(BASE_URL + url)
        .then(res => {
            if (!res.ok) {
                throw new Error(res.status);
            }
            return res.json();
        })
        .catch(err => {
            console.log('Problem during fetch: ', err);
            return null;
        })
    ));
}

export async function fetchLocations(name) {
    return await getRequest(
        "locations/?term=" + encodeURIComponent(name)
    );
}

export async function fetchFlights(flyFrom, flyTo, dateFrom, dateTo, limit) {
    const flyFromURI = encodeURIComponent(flyFrom.map(place => place.id).join(","));
    const flyToURI = encodeURIComponent(flyTo.map(place => place.id).join(","));

    const dateFromURI = dateFrom
        ? encodeURIComponent(dateFrom.format(DATE_FORMAT))
        : "";
    const dateToURI = dateTo
        ? encodeURIComponent(dateTo.format(DATE_FORMAT))
        : "";

    const url = `flights?v3&flyFrom=${flyFromURI}&to=${flyToURI}`
        + `&dateFrom=${dateFromURI}&dateTo=${dateToURI}`
        + "&limit=" + parseInt(limit, 10);

    return await getRequest(url);
}
