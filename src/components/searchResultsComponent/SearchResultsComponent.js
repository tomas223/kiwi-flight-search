import React, {Component} from 'react';
import {Row, Col, Container} from 'reactstrap';
import SearchResultItem from './SearchResultItem';
import PaginationComponent from './PaginationComponent';

const calcPageCount = (data) =>
    data && data.length > 0
        ? Math.ceil(data.length / 5)
        : 0;

class SearchBarComponent extends Component {
    handlePageClick = (page) => {
        const {onPageChange} = this.props;
        onPageChange(page.selected);
    };

    constructor(props) {
        super(props);
        this.state = {
            pageCount: 0,
            resultsPerPage: 5
        }
    }

    sliceResults() {
        const {resultsPerPage} = this.state;
        const {currentPage} = this.props;
        const {flights} = this.props;
        const start = currentPage * resultsPerPage;
        return flights && flights.slice(start, start + resultsPerPage);
    }

    componentWillReceiveProps(nextProps) {
        const {flights} = nextProps;
        this.setState({pageCount: calcPageCount(flights)});
    }

    componentWillMount() {
        const {flights} = this.props;
        this.setState({pageCount: calcPageCount(flights)});
    }

    render() {
        const {pageCount} = this.state;
        const {currentPage, isFetching} = this.props;

        return (
            <div className="search-results">
                <Container>
                    <Row className="results-header">
                        <Col xs={12}>
                            <PaginationComponent
                                pageCount={pageCount}
                                onPageChange={this.handlePageClick}
                                forcePage={currentPage}
                                isFetching={isFetching}
                            />
                        </Col>
                    </Row>
                    <Row className="results-main">
                        {
                            this.sliceResults().map((route, index) =>
                                <Col xs={12} key={index}>
                                    <SearchResultItem
                                        route={route}
                                    />
                                </Col>
                            )
                        }
                    </Row>
                    <Row className="results-footer">
                    </Row>
                </Container>
            </div>
        );
    }
}

export default SearchBarComponent;
