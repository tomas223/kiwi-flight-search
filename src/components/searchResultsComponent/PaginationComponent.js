import React from 'react';
import ReactPaginate from 'react-paginate/dist/';

const PaginationComponent = ({onPageChange, pageCount, forcePage, isFetching}) => {
    const nextLabel = isFetching ? "loading" : "next";

    return pageCount && pageCount > 1 ?
        <div className="pagination-wrapper">
            <ReactPaginate previousLabel={"prev"}
                           nextLabel={nextLabel}
                           breakLabel="..."
                           breakClassName={"break-me"}
                           pageCount={pageCount}
                           marginPagesDisplayed={1}
                           pageRangeDisplayed={3}
                           onPageChange={onPageChange}
                           containerClassName={"pagination"}
                           subContainerClassName={"pages pagination"}
                           forcePage={forcePage}
                           activeClassName={"active"}/>
        </div>
        : null;
};

export default PaginationComponent;
