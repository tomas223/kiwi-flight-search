import React from 'react';
import {Card, Row, Col} from 'reactstrap';
import moment from 'moment';

const SearchResultItem = ({route}) => {
    const {dTime} = route;
    const time = moment(dTime*1000).format("ddd, DD MMM HH:mm");
    return (
        <Card>
            <Row>
                <Col xs={4} sm={2} className="price">
                    <div>{route.conversion.EUR} EUR</div>
                </Col>
                <Col xs={8} sm={5} className="route">
                    <span>{route.cityFrom} ({route.countryFrom.code})</span> -> <span>{route.cityTo} ({route.countryTo.code})</span>
                </Col>
                <Col className="travel-time">
                    <div>{time}</div>
                    <div>{route.fly_duration}</div>
                </Col>
            </Row>
        </Card>
    );
};

export default SearchResultItem;
