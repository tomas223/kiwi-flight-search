import React, {Component} from 'react';
import {DateRangePicker} from 'react-dates';
import {tabletViewportSize} from '../../etc/appConfig';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';


class DateInputComponent extends Component {
    state = {
        startDate: null,
        months: 1

    };

    onDatesChange = (startDate, endDate) => {
        const {onDatesChange} = this.props;
        onDatesChange({
            startDate, endDate
        });
    };

    componentDidMount() {
        this.updateResponsive();

    }

    updateResponsive() {
        const months = window.innerWidth > tabletViewportSize
            ? 2
            : 1;
        this.setState({
            months
        });
    }

    render () {
        const {startDate, endDate} = this.props;
        const {focusedInput, months} = this.state;
        return (
            <div className="date-selector">
                <DateRangePicker
                    startDate={startDate}
                    startDateId="start-date-id"
                    endDate={endDate}
                    endDateId="end-date-id"
                    onDatesChange={({ startDate, endDate }) => this.onDatesChange(startDate, endDate)}
                    focusedInput={focusedInput}
                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                    numberOfMonths={months}
                    orientation="horizontal"
                    minimumNights={0}
                />
            </div>
        );
    }
}

export default DateInputComponent;