import React from 'react';
import Select from "react-select";
import {fetchLocations} from "../../utils/fetchUtils";

function LocationInput({placeholder, value, autoFocus, onChange}) {
    const getOptions = async (input) => {
        const data = await fetchLocations(input);
        const options = data && data.locations.map(place => {
            const label = place.country
                ? `${place.name}, ${place.country.name}`
                : place.name;
            return {
                id: place.id,
                label
            }
        });
        return {options};
    };

    return (
        <div className="location-select">
            <Select.Async
                loadOptions={getOptions}
                placeholder={placeholder}
                autoFocus={autoFocus}
                multi={true}
                value={value}
                onChange={onChange}
                onBlurResetsInput={false}
                onCloseResetsInput={false}
                valueKey="id" labelKey="label"
            />
        </div>
    );
}

export default LocationInput;