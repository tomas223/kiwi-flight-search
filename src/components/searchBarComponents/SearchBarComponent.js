import React from 'react';
import {Row, Col, Container, Button} from 'reactstrap';

import LocationInput from './LocationInput';
import DateInputComponent from './DateInputComponent';


const SearchBarComponent = ({onChange, onSearch, isFetching, searchParams}) => {
    const {flyFrom, flyTo, startDate, endDate} = searchParams;

    return (
        <div className="search-bar">
            <Container>
                <Row className="location">
                    <Col xs={12} md={6}>
                        <LocationInput
                            placeholder="Fly from"
                            onChange={flyFrom => onChange({flyFrom})}
                            value={flyFrom}
                            autoFocus={true}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <LocationInput
                            placeholder="Fly to"
                            onChange={flyTo => onChange({flyTo})}
                            value={flyTo}
                        />
                    </Col>
                </Row>
                <Row className="date-submit">
                    <Col xs={12} md={6}>
                        <DateInputComponent
                            onDatesChange={dates => onChange({...dates})}
                            startDate={startDate}
                            endDate={endDate}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <div className="button-wrap">
                            <Button
                                onClick={() => onSearch()}
                            >
                                    <span>
                                    {
                                        isFetching
                                            ? "Searching..."
                                            : "Search flights"
                                    }
                                    </span>
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
};

export default SearchBarComponent;
