import React, {Component} from 'react';

import SearchBarComponent from '../components/searchBarComponents/SearchBarComponent';
import SearchResultsComponent from '../components/searchResultsComponent/SearchResultsComponent';
import {fetchFlights} from "../utils/fetchUtils";
import {flightSearch} from "../etc/appConfig";

class SearchPage extends Component {
    state = {
        newSearch: flightSearch,
        currentSearch: flightSearch,
        searchLimit: 30,
        currentPage: 0,
        searchResults: [],
        isFetchingNew: false,
        isFetchingMore: false,
    };

    updateNewSearch = (param) => {
        this.setState(prevState => ({
            newSearch: {
                ...prevState.newSearch,
                ...param,
            }
        }));
    };

    nextPage = async (pageNum) => {
        this.setState({currentPage: pageNum});
        const {searchLimit, currentSearch} = this.state;

        if (pageNum * 5 > searchLimit - 20) {
            this.setState({searchLimit: searchLimit + 30, isFetchingMore: true});
            await this.getFlights(currentSearch, searchLimit + 30);
            this.setState({isFetchingMore: false});
        }
    };

    newSearch = async () => {
        const {newSearch} = this.state;
        this.setState({
            searchLimit: 30,
            isFetchingNew: true,
            currentSearch: {
                ...newSearch
            }
        });
        await this.getFlights(newSearch, 30);
        this.setState({
            currentPage: 0,
            isFetchingNew: false,
        });
    };

    getFlights = async (flightSearch, searchLimit) => {
        const {flyFrom, flyTo, startDate, endDate} = flightSearch;
        const flights = await fetchFlights(flyFrom, flyTo, startDate, endDate, searchLimit);
        const searchResults = (flights && flights.data) || [];

        this.setState({ searchResults });
    };

    render() {
        const {
            searchLimit, searchResults, currentPage, newSearch, isFetchingNew, isFetchingMore
        } = this.state;

        return (
            <div className="search-page">

                <SearchBarComponent
                    onSearch={() => this.newSearch()}
                    onChange={param => this.updateNewSearch(param)}
                    searchParams={newSearch}
                    searchLimit={searchLimit}
                    isFetching={isFetchingNew}
                />
                <SearchResultsComponent
                    flights={searchResults}
                    currentPage={currentPage}
                    isFetching={isFetchingMore}
                    onPageChange={page => this.nextPage(page)}
                />

            </div>
        );
    }
}

export default SearchPage;
